FROM node:10-slim

WORKDIR /app

RUN yarn global add @vue/cli

COPY ./entrypoint /

ENTRYPOINT ["/entrypoint"]
