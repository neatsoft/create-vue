# create-vue

## Build docker image and push it to the dockerhub

    docker build -t neatsoft/create-vue .
    docker login
    docker push neatsoft/create-vue

## Create new project

    docker run -it -u `id -u`:`id -g` -v `pwd`:/app neatsoft/create-vue <project_name>
